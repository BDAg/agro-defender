import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { tap, map, filter, distinctUntilChanged, debounceTime, switchMap } from 'rxjs/operators';
import { SearchService } from '../services/search.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  form: FormGroup;
  search$: Observable<any>;

  results = [];

  constructor(
    private formBuilder: FormBuilder,
    private searchService: SearchService
  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      search: ['']
    });

    this.form.get('search').valueChanges
      .pipe(
        map(data => data.trim()),
        filter(data => data.length > 3),
        debounceTime(200),
        distinctUntilChanged(),
        map(data => data.toUpperCase()),
        switchMap(data => this.searchService.search(data)),
        map(data => this.results = data),
      ).subscribe();
  }
}
