import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(
    private http: HttpClient
  ) { }

  public search(search) {
    return this.http.get('/assets/crawler/pdfs.json')
      .pipe(
        map((data: { pdfs: any[] }) => data.pdfs),
        map((data: any) => data.filter(value => value.nome.includes(search)))
      );
  }
}
