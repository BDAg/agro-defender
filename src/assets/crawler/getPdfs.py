from  bs4 import BeautifulSoup

from lxml import html

import requests

import csv

import time

import json

begin = time.time()

initialUrl = 'http://celepar07web.pr.gov.br/agrotoxicos/bulas.asp'

categorias = ['Inseticida', 'Herbicida', 'Fungicida', 'Outros']

allUrls = []
allUrlsJson = {}
allUrlsJson['links'] = [];

page = requests.get(initialUrl)

soup = BeautifulSoup(page.content, 'html.parser')

initialP = soup.find('p')
countInitialLinks = 0

pdfLinksUrl = []
pdfLinksUrlJson = {}
pdfLinksUrlJson['pdfs'] = []

for link in initialP.find_all('a'):    
    allUrlsJson['links'].append({
        "categoria": categorias[countInitialLinks],
        "link": link.get('href')
    })
    
    allUrls.append(link.get('href'))
    countInitialLinks += 1    

with open('links.json', 'w') as json_file:
    json.dump(allUrlsJson, json_file)

countPdf = 0
for link in allUrls:
    page = requests.get(link)
    soup = BeautifulSoup(page.content, 'html.parser')

    divPage = soup.select('div#page a') 

    for linkPdf in divPage:
        pdfLinksUrlJson['pdfs'].append({
            "nome": linkPdf.get_text(),
            "categoria": categorias[countPdf],
            "pdfLink": linkPdf.get('href')
        })        
    
    countPdf += 1

with open('pdfs.json', 'w') as json_file:
    json.dump(pdfLinksUrlJson, json_file)